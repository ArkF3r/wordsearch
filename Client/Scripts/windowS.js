var client = new net.Socket();
client.connect(9001, 'localhost', function() {
    alert('¡Conectado a Juego!');
});
const timer = ms => new Promise(res => setTimeout(res, ms));

$(document).ready(function() {
    var init = ""
    var estado = 0
    var a = 15
    var manejadorMensaje = function(mensaje) { //Recibe un objeto
        console.log(mensaje);
        if (mensaje.Tipo === 'Datosjuego') {
            var $title = $("#Concepto")
            $title.text(mensaje.Concepto)
            llenarTabla(mensaje.Tablero)
            if (mensaje.Dificultad === 'Facil') {
                console.log(mensaje.Palabras);
                var $section = $("#Palabras");
                $section.removeClass("is-hidden");
                var $h = $("#palabrasTitle");
                $h.text('Palabras');
                var $p = $("#listaPalabras");
                $p.append(mensaje.Palabras.toString());
            }
            if (mensaje.Modalidad === 'Anagrama') {
                var $section = $("#Palabras");
                $section.removeClass("is-hidden");
                var $h = $("#palabrasTitle");
                $h.text('Anagramas');
                var $p = $("#listaPalabras");
                $p.append(mensaje.Anagramas.toString());
            }
        } else if (mensaje.Tipo === 'RespuestaVerificacion') {
            palabraverificada(mensaje.Estado);
            if (mensaje.Dificultad === 'Intermedia' && !mensaje.Estado) {
                var $section = $("#Palabras");
                $section.removeClass("is-hidden");
                var $h = $("#palabrasTitle");
                $h.text('Palabras');
                var $p = $("#listaPalabras");
                $p.append(`${mensaje.PalabraPista}, `);
            }
            console.log('Verificacion');
        } else if (mensaje.Tipo === 'Finjuego') {
            palabraverificada(true);
            alert(`Tiempo: ${mensaje.Time} minutos\nModalidad: ${mensaje.Modalidad}\nDificultad: ${mensaje.Dificultad}`)
            cambiarBotones()
            console.log('FIN');
        } else if (mensaje.Tipo === 'Puntuaciones') {
            var ranking = mensaje.Puntuaciones;
            if (ranking.length > 0) {
                var texto = ""; //"Jugador\t\tTiempo\t\tDificultad\t\tModalidad\n";
                var i = 0;
                for (var e of ranking) {
                    texto = `${texto}${(i + 1)}.-  Jugador: ${e.Nick}  Tiempo: ${e.Tiempo}  Dificultad: ${e.Dificultad}  Modalidad: ${e.Modalidad}\n`;
                    i += 1;
                }
                alert(texto)
            } else {
                alert('No hay mejores puntuaciones aún')
            }
        } else if (mensaje.Tipo === 'Rendicion') {
            console.log('Rendicion');
            llenarTabla(mensaje.Solucion)
            cambiarBotones()
        }
    }

    var server = new clientServer(client); //Creamos un server
    server.InitTCP(manejadorMensaje)
    genTabla(a, a)

    $("td").click(function(e) {
        var $celda = $(e.target)
        console.log($celda.hasClass("is-info") && estado == 0)
        if ($celda.hasClass("is-info") && estado == 0) {
            init = ""
            $celda.removeClass("is-info")
            $celda.removeClass("OK")
        } else {
            if (estado != 1) {
                if (init === "") {
                    init = $celda.attr('id')
                    $celda.addClass("is-info")
                    $celda.addClass("OK")
                } else {
                    var initX = obtenerX(init)
                    var initY = obtenerY(init)
                    var nX = obtenerX($celda.attr('id'))
                    var nY = obtenerY($celda.attr('id'))
                    if (initX == nX) {
                        if (marcafila(initX, initY, nY, 1) === true) {
                            estado = 1
                        }
                    } else {
                        if (initY == nY) {
                            if (marcafila(initY, initX, nX, 2) === true) {
                                estado = 1
                            }
                        } else {
                            if (Math.abs(initX - nX) == Math.abs(initY - nY)) {
                                if (marcaDiagonal(initX, initY, nX, nY) === true) {
                                    estado = 1
                                }
                            }
                        }
                    }
                }
            }
        }
    })
    $("td").mouseover(function(e) {
        var $celda = $(e.target)
    })

    $("#Mod").on('change', function() {
        var selectMod = $('#Mod').val();
        var $CDif = $('#CDif');
        if (selectMod === 'Anagrama') {
            $CDif.addClass('is-hidden');
        } else {
            $CDif.removeClass('is-hidden');
        }
    });

    $("#Enviar").click(function() {
        estado = 1
    })
    $("#Limpiar").click(function() {
        $("td").removeClass("is-info")
        $("td").removeClass("is-danger")
        estado = 0
        init = ""
    })

    function genTabla(a, b) {
        var $tabla = $("#tbody1")
        for (var i = 0; i < a; i++) {
            $tabla.append("<tr>")
            for (var j = 0; j < b; j++) {
                $tabla.append("<td id='" + i + ":" + j + "'>" + i + ":" + j + "</td>")
            }
            $tabla.append("</tr>")
        }
    }

    function marcafila(c0, c1, c2, t) {
        if (t == 1) {
            if (c1 <= c2) {
                for (var i = c1; i <= c2; i++) {
                    var a = "#" + c1 + ":" + i
                    $("[id='" + c0 + ":" + i + "']").addClass("is-info")
                }
            } else {
                for (var i = c2; i <= c1; i++) {
                    var a = "#" + c1 + ":" + i
                    $("[id='" + c0 + ":" + i + "']").addClass("is-info")
                }
            }
        } else {
            if (c1 <= c2) {
                for (var i = c1; i <= c2; i++) {
                    var a = "#" + c1 + ":" + i
                    $("[id='" + i + ":" + c0 + "']").addClass("is-info")
                }
            } else {
                for (var i = c2; i <= c1; i++) {
                    var a = "#" + c1 + ":" + i
                    $("[id='" + i + ":" + c0 + "']").addClass("is-info")
                }
            }
        }
        init = ""
        return true
    }

    function marcaDiagonal(x1, y1, x2, y2) {
        if (x1 <= x2) {
            if (y1 <= y2) {
                for (var i = 0; i <= x2 - x1; i++) {
                    for (var j = 0; j <= y2 - y1; j++) {
                        if (i == j) {
                            $("[id='" + (x1 + i) + ":" + (y1 + j) + "']").addClass("is-info")
                        }
                    }
                }
            } else {
                for (var i = 0; i <= x2 - x1; i++) {
                    for (var j = 0; j <= y1 - y2; j++) {
                        if (i == j) {
                            $("[id='" + (x1 + i) + ":" + (y1 - j) + "']").addClass("is-info")
                        }
                    }
                }
            }
        } else {
            if (y1 <= y2) {
                for (var i = 0; i <= x1 - x2; i++) {
                    for (var j = 0; j <= y2 - y1; j++) {
                        if (i == j) {
                            $("[id='" + (x1 - i) + ":" + (y1 + j) + "']").addClass("is-info")
                        }
                    }
                }
            } else {
                for (var i = 0; i <= x1 - x2; i++) {
                    for (var j = 0; j <= y1 - y2; j++) {
                        if (i == j) {
                            $("[id='" + (x1 - i) + ":" + (y1 - j) + "']").addClass("is-info")
                        }
                    }
                }
            }
        }
        init = ""
        return true
    }

    function obtenerX(coor) {
        return parseInt((coor).split(":")[0])
    }

    function obtenerY(coor) {
        return parseInt((coor).split(":")[1])
    }

    $('#Comenzar').click(function() {
        var Nick = $("#Nick").val()
        if (Nick.length > 0) {
            $("#Nick").prop('disabled', true);
            var selectedMod = $("#Mod").val();
            var selectedDif = $("#Dif").val();
            if (selectedMod === 'Anagrama') {
                selectedDif = 'Unica'
            }
            Mensaje = {
                Modalidad: selectedMod,
                Dificultad: selectedDif,
                Nick: Nick
            }
            server.EnviaTCP('Nuevojuego', Mensaje);
            var $section = $("#MATRIZ");
            $section.removeClass('is-hidden');
            $section = $("#Datos");
            $section.addClass('is-hidden');
        } else {
            alert('Ingrese un Nickname')
        }
    });

    $("#RENDIR").click(function() {
        server.EnviaTCP('Rendicion');
    })

    $("#Punt").click(function() {
        server.EnviaTCP('Puntuaciones');
    })

    $("#Enviar").click(function() {
        var z = $("td.is-info")
        var palabra = ""
        for (var td of z) {
            palabra = palabra + $(td).html()
        }
        console.log(palabra);
        var Mensaje = {
            Palabra: palabra
        };
        server.EnviaTCP('Verificapalabra', Mensaje);
    })

    $("#JugarA").click(function() {
        var tds = $("td")
        init = "";
        estado = 0
        for (var td of tds) {
            var $celda = $(td)
            $celda.removeClass("is-success");
            $celda.removeClass("is-info");
            $celda.removeClass("is-danger");
            $celda.removeClass("OK");
        }
        var $section = $("#MATRIZ");
        $section.addClass('is-hidden');
        $section = $("#Datos");
        $section.removeClass('is-hidden');
        var $button = $('#JugarA');
        $button.addClass('is-hidden');
        var $button = $('#Punt');
        $button.addClass('is-hidden');
        var $button = $('#Salir');
        $button.addClass('is-hidden');
        var $button = $('#Enviar');
        $button.removeClass('is-hidden');
        var $button = $('#Limpiar');
        $button.removeClass('is-hidden');
        var $button = $('#RENDIR');
        $button.removeClass('is-hidden');
        var $section = $("#Palabras");
        $section.addClass("is-hidden");
        var $p = $("#listaPalabras");
        $p.text('');
    })

    $("#Salir").click(function() {
        location.reload(true);
    })

    function llenarTabla(letras) {
        var $tabla = $("#tbody1");
        $tabla.removeClass('is-hidden');
        for (var i = 0; i < a; i++) {
            for (var j = 0; j < a; j++) {
                $('[id="' + i + ':' + j + '"]').html(letras[i][j]);
            }
        }
    }

    function palabraverificada(Edo) {
        var z = $("td.is-info");
        if (Edo) {
            for (var td of z) {
                $(td).removeClass("is-info");
                $(td).removeClass("OK");
                $(td).addClass("is-success");
                init = "";
                estado = 0;
            }
        } else {
            for (var td of z) {
                $(td).removeClass("is-info");
                $(td).addClass("is-danger");
            }
        }
    }

    function cambiarBotones() {
        var $button = $('#JugarA');
        $button.removeClass('is-hidden');
        var $button = $('#Punt');
        $button.removeClass('is-hidden');
        var $button = $('#Salir');
        $button.removeClass('is-hidden');
        var $button = $('#Enviar');
        $button.addClass('is-hidden');
        var $button = $('#Limpiar');
        $button.addClass('is-hidden');
        var $button = $('#RENDIR');
        $button.addClass('is-hidden');
    }
})