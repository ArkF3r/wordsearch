class creadorMensaje {
    constructor() {
        this.mensaje = undefined;
    }
    creaMensaje(Tipo, Mensaje) {
        var obj = {}
        if (Tipo === 'Nuevojuego') {
            obj = {
                Tipo: Tipo,
                Modalidad: Mensaje.Modalidad,
                Dificultad: Mensaje.Dificultad,
                Nick: Mensaje.Nick
            };
        } else if (Tipo === 'Verificapalabra') {
            obj = {
                Tipo: Tipo,
                Palabra: Mensaje.Palabra
            };
        } else if (Tipo === 'Puntuaciones') {
            obj = {
                Tipo: Tipo
            };
        } else if (Tipo === 'Rendicion') {
            obj = {
                Tipo: Tipo
            };
        }
        this.mensaje = JSON.stringify(obj);
    }
    getMensaje() {
        return this.mensaje;
    }
}