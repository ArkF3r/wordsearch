var net = require('net');
class clientServer {
    constructor(server) {
        this.messageMaker = new creadorMensaje();
        this.socketClienteTCP = server; //socketTCP
    }

    InitTCP(callback) {
        global.callback = callback;
        var client = this.socketClienteTCP;
        var self = this;
        client.on('data', function(data) {
            easysocket.recieve(client, data, self.mensajeTCP);
        });
        client.on('close', function() {
            console.log('Connection closed');
        });
    }

    mensajeTCP(socket, buffer) {
        try {
            var myObj = JSON.parse(buffer.toString());
            callback(myObj);
        } catch (e) {
            return
        }

    }

    EnviaTCP(Tipo, mensaje) {
        this.messageMaker.creaMensaje(Tipo, mensaje);
        var m = this.messageMaker.getMensaje();
        easysocket.send(this.socketClienteTCP, m, function(client) {
            console.log("Mensaje enviado --> " + m);
        });
    }
}