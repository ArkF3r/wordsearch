import random
class Tablero:
    def __init__(self, width, height):
        self.letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';            #letters used for filler
        self.MAXATTEMPTS = 20;                                  #maximum amount of times to place a word
        self.width = +width or 20;
        self.height = +height or 20;

    def creategrid(self, words):
        if ( not words or not len(words) ):
            return False;
        self.opts = .5#opts || {};
        self.words = words
        self.words.sort(key = len, reverse = True) #Ordenar de mayor a menor longitud
        self.grid = [[' ' for x in range(self.width)] for y in range(self.height)]
        self.solved = [[' ' for x in range(self.width)] for y in range(self.height)]
        #loop the words
        self.unplaced = []
        i = 0
        for i in range( len(self.words) ):
            word = originalword = self.words[i]
            #reverse the word if is needed
            if( random.uniform(0, 1) < self.opts ):
                word = ''.join(reversed(word))
            Intentos = 0
            while ( Intentos < self.MAXATTEMPTS ):
                #determine the direction (up-right, right, down-right, down)
                direction = random.randint(0, 3);
                info = self.directioninfo(word, direction, self.width, self.height);
                if (info['maxx'] < 0 or info['maxy'] < 0 or info['maxy'] < info['miny'] or info['maxx'] < info['minx'] ):
                    self.unplaced.append(originalword);
                    break;
                x = ox = round(random.uniform(0,1) * (info['maxx'] - info['minx']) + info['minx']);
                y = oy = round(random.uniform(0,1) * (info['maxy'] - info['miny']) + info['miny']);
                
                #check to make sure there are no collisions
                placeable = True
                count = l = 0
                for  l in range(len(word)) : 
                    charingrid = self.grid[y][x]
                    if( charingrid != ' '): #check if there is a character in the grid
                        if ( charingrid != word[l] ):
                            #not the same latter, try again
                            placeable = False; # :(
                            break
                        else:
                            #same letter! count it
                            count += 1
                    #keep trying!
                    y += info['dy']
                    x += info['dx']
                if ( not placeable or count >= len(word)):
                    Intentos += 1
                    continue
                #the word was placeable if we make it here!
                #reset x and y and place it
                x = ox
                y = oy;
                l = 0
                for l in range(len(word)):
                    self.solved[y][x] = word[l];
                    self.grid[y][x] = word[l];
                    y += info['dy']
                    x += info['dx']
                break
            if (Intentos >= 20):
                self.unplaced.append(originalword)
        #put in filler characters
        i = 0
        for i in range(len(self.grid)):
            j = 0
            for j in range(len(self.grid[i])):
                if (self.grid[i][j] == ' '):
                    self.grid[i][j] = self.letters[random.randint(0, len(self.letters) - 1)]

    def directioninfo(self, word, direction, width, height ):
        #determine the bounds
        minx = miny = 0;
        maxx = width - 1;
        maxy = height - 1;
        dx = dy = 0;
        if (direction == 0): #up-right
            maxy = height - 1
            miny = len(word) - 1
            dy = -1
            maxx = width - len(word)
            minx = 0
            dx = 1
        elif (direction == 1): #right
            maxx = width - len(word)
            minx = 0
            dx = 1
        elif (direction == 2): #down-right
            miny = 0
            maxy = height - len(word)
            dy = 1
            maxx = width - len(word)
            minx = 0
            dx = 1
        elif (direction == 3): #down
            miny = 0
            maxy = height - len(word)
            dy = 1
        return {
            'maxx': maxx,
            'maxy': maxy,
            'minx': minx,
            'miny': miny,
            'dx': dx,
            'dy': dy
        }

    def getGrid(self):
        return self.grid

    def getSolution(self):
        return self.solved

    def getUnplace(self):
        return self.unplaced
# def main():
#     a = ['LIMON', 'SANDIA', 'NARANJA', 'DURAZNO', 'MELON', 'FRESA', 'GUAYABA', 'MANZANA', 'MANDARINA', 'ZARZAMORA', 'PERA', 'LIMA', 'PLATANO', 'UVA']
#     b = 15
#     c = Tablero(b, b)
#     c.creategrid(a)
#     print(c.getUnplace())
#     for i in c.getGrid():
#         print(i)
#     print('\nThe solution is: \n\n')
#     for j in c.getSolution():
#         print(j)
#         print()
# main()