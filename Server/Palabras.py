import random
class Palabras:
    def __init__(self, Tipo, numPalabras):
        self.i = 0
        self.Tipo = Tipo
        self.choosenWords = self.selectWords(numPalabras)
        self.Anagramas = self.doAnagrams()
    
    def selectWords(self, numPalabras):
        file = None
        if(self.Tipo == 0):
            file = open('./Dicts/Animales.dic', 'r')
            self.Concept = 'ANIMALES'
        elif(self.Tipo == 1):
            file = open('./Dicts/Colores.dic', 'r')
            self.Concept = 'COLORES'
        elif(self.Tipo == 2):
            file = open('./Dicts/Comida_mexicana.dic', 'r')
            self.Concept = 'COMIDA MEXICANA'
        elif(self.Tipo == 3):
            file = open('./Dicts/Flores.dic', 'r')
            self.Concept = 'FLORES'
        elif(self.Tipo == 4):
            file = open('./Dicts/Frutas.dic', 'r')
            self.Concept = 'FRUTA'
        Pal = file.read()
        Pal = Pal.split('\n')
        return  random.sample(Pal, numPalabras)

    
    def doAnagrams(self):
        Anagrams = []
        for p in self.choosenWords:
            anagram = random.sample(p, len(p))
            anagram = ''.join(anagram)
            Anagrams.append(anagram)
        return Anagrams

    def getConcept(self):
        return self.Concept

    def getPalabras(self):
        return self.choosenWords

    def getAnagramas(self):
        return self.Anagramas
    
    def hasWord(self, word):
        return word in self.choosenWords
    
    def getNextWord(self):
        S = 0
        if(self.i < len(self.choosenWords)):
            S = self.choosenWords[self.i]
            self.i = self.i + 1
        return S

    