from time import time
from Tablero import Tablero
from Palabras import Palabras
from time import sleep
import random
import json
class Juego:
    def __init__(self):
        self.size = 15
        self.Tablero = Tablero(self.size, self.size)
        self.numPalabras = 14
        self.Palabras = None
        self.Modalidad = None
        self.Dificultad = None
        self.scoreBoard = []

    def nuevoJuego(self, Modalidad, Dificultad, Nick ):
        self.Modalidad = Modalidad
        self.Dificultad = Dificultad
        self.Nick = Nick
        tipoConcepto = random.randint(0,4)
        self.Palabras = Palabras(tipoConcepto, self.numPalabras)
        self.palabrasRestantes = self.numPalabras
        self.timer = time()
        self.Tablero.creategrid(self.Palabras.getPalabras())
        self.solution()
        return self.creaMensaje('Datosjuego')

    def solution(self):
        a = self.Tablero.getSolution()
        for x in a:
            print(x)
            print()

    def verificaPalabra(self, Palabra):
        self.stateLastWord = self.Palabras.hasWord(Palabra) or self.Palabras.hasWord(''.join(reversed(Palabra)))
        if ( self.stateLastWord ):
            self.palabrasRestantes = self.palabrasRestantes - 1
            if ( self.palabrasRestantes == 0 ):
                self.lastTime = time() - self.timer #Obtener tiempo
                self.lastTime = round( self.lastTime/60, 2) #En minutos
                self.verificaScore()
                return self.creaMensaje('Finjuego')
        return self.creaMensaje('RespuestaVerificacion')

    def creaMensaje(self, Tipomensaje):
        Mensaje = {}
        if(Tipomensaje == 'Datosjuego'):
            Mensaje = { 
                'Tipo': Tipomensaje,
                'Concepto': self.Palabras.getConcept(),
                'Tablero': self.Tablero.getGrid(),
                'Dificultad': self.Dificultad,
                'Modalidad': self.Modalidad
            }
            if(self.Dificultad == 'Facil'):
                Mensaje['Palabras'] = self.Palabras.getPalabras()
            if(self.Modalidad == 'Anagrama' ):
                Mensaje['Anagramas'] = self.Palabras.getAnagramas()
        elif( Tipomensaje == 'RespuestaVerificacion' ):
            Mensaje = {
                'Tipo': Tipomensaje,
                'Estado': self.stateLastWord,
                'Dificultad': self.Dificultad,
                'Modalidad': self.Modalidad
            }
            if( self.Dificultad == 'Intermedia' and not self.stateLastWord):
                Mensaje['PalabraPista'] = self.Palabras.getNextWord()
        elif( Tipomensaje == 'Finjuego' ):
            Mensaje = {
                'Tipo': Tipomensaje,
                'Time': self.lastTime,
                'Modalidad': self.Modalidad,
                'Dificultad': self.Dificultad
            }
        elif( Tipomensaje == 'Puntuaciones' ):
            Mensaje = {
                'Tipo': Tipomensaje,
                'Puntuaciones': self.scoreBoard
            }
        elif( Tipomensaje == 'Rendicion' ):
            Mensaje = {
                'Tipo': Tipomensaje,
                'Solucion': self.Tablero.getSolution(),
                'Time': self.lastTime
            }
        Mensaje = json.dumps(Mensaje)
        return Mensaje
        
    def verificaScore(self):
        scoreAux = self.scoreBoard.copy()
        newScore = {'Nick': self.Nick, 'Tiempo':self.lastTime, 'Dificultad': self.Dificultad, 'Modalidad':self.Modalidad}
        if(len(self.scoreBoard) == 0):
            self.scoreBoard.append(newScore)
        else:
            i = 0
            inserted = False
            for a in self.scoreBoard:
                if i == 10:
                    break
                if( self.lastTime < a['Tiempo'] and not inserted):
                    scoreAux.insert(i, newScore )
                    inserted = True
                i += 1
            if not inserted:
                scoreAux.append(newScore)
            self.scoreBoard = scoreAux[:10]

    def getScores(self):
        return self.creaMensaje('Puntuaciones')
    
    def Rendicion(self):
        self.lastTime = time() - self.timer 
        self.lastTime = round( self.lastTime/60, 2) #En minutos
        return self.creaMensaje('Rendicion')